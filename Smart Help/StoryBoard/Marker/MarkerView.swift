//
//  MarkerView.swift
//  Smart Help
//
//  Created by thanh tuan on 6/1/19.
//  Copyright © 2019 SmartHelp. All rights reserved.
//

import UIKit

class MarkerView: UIView {

    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initView()
        
    }
    func initView() {
        Bundle.main.loadNibNamed("MarkerView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
    }
    
    func loadData(price: Double) {
        bgView.layer.cornerRadius = 15
        bgView.layer.masksToBounds = true
        priceLabel.text = price.numberHexString() + "/kWh"
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}
extension String {
    var length: Int {
        return self.count
    }
}
extension Double {
    func numberHexString() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.currency
        numberFormatter.locale = Locale(identifier: "vi_VN")
        let formattedNumber = numberFormatter.string(from: NSNumber(value: self))
        
        //        let str = formattedNumber?.stringOnlyNumber()
        //        return String(format: "%@ VND", str ?? 0)
        
        //        return formattedNumber ?? kEmptyStringField
        
        if let format = formattedNumber {
            let regex = try! NSRegularExpression(pattern: "[^0-9.,]", options: .caseInsensitive)
            let formatedStr = regex.stringByReplacingMatches(in: format, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSMakeRange(0, format.length), withTemplate: "")
            return String(format: "%@ VND", formatedStr)
        } else {
            return "0 VND"
        }
        
    }
    
    func numberHexStringNotCurrency() -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        numberFormatter.locale = Locale(identifier: "vi_VN")
        let formattedNumber = numberFormatter.string(from: NSNumber(value: self))
        return formattedNumber ?? "0 VND"
    }
    
    func formatToString() -> String {
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        
        // Avoid not getting a zero on numbers lower than 1
        // Eg: .5, .67, etc...
        formatter.numberStyle = .decimal
        return formatter.string(from: self as NSNumber ) ?? "0"
    }
}
