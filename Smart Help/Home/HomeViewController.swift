//
//  HomeViewController.swift
//  Smart Help
//
//  Created by Hiên Hiên on 6/2/19.
//  Copyright © 2019 SmartHelp. All rights reserved.
//

import UIKit
import SwiftChart
import PNChart

class HomeViewController: UIViewController {

    @IBOutlet weak var pieChart: UIView!
    @IBOutlet weak var lineChart: PNLineChart!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var pieView: UIView!
    @IBOutlet weak var segmentControl: UISegmentedControl! {
        didSet {
            let font: [AnyHashable : Any] = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 10)]
            segmentControl.setTitleTextAttributes(font as? [NSAttributedString.Key : Any], for: .normal)
        }
    }
    var pie: PNPieChart!
    let usedItems = [PNPieChartDataItem(value: 22, color: .red, description: "Light"),
                     PNPieChartDataItem(value: 25, color: .blue, description: "Air Conditioner"),
                     PNPieChartDataItem(value: 10.5, color: .orange, description: "Tivi"),
                     PNPieChartDataItem(value: 15.5, color: .purple, description: "Fridge"),
                     PNPieChartDataItem(value: 17, color: .magenta, description: "Washing Machine"),
                     PNPieChartDataItem(value: 10, color: .gray, description: "Others")
    ]
    let madeItems = [PNPieChartDataItem(value: 50, color: .red, description: "Solar"),
                     PNPieChartDataItem(value: 30, color: .blue, description: "Wind"),
                     PNPieChartDataItem(value: 20, color: .gray, description: "Others")
    ]
    let storedItems = [PNPieChartDataItem(value: 70, color: .red, description: "Solar Battery"),
                       PNPieChartDataItem(value: 15, color: .blue, description: "Car Battery"),
                       PNPieChartDataItem(value: 5, color: .purple, description: "Motocycle Battery"),
                       PNPieChartDataItem(value: 10, color: .gray, description: "Others")
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentControl.addTarget(self, action: #selector(segmentedControlValueChanged(segment:)), for: .valueChanged)
        drawLineChart()
        drawPieChart(items: usedItems as [Any])
        
    }
    
    func drawPieChart(items: [Any]) {
        for subview in pieChart.subviews {
            subview.removeFromSuperview()
        }
        if (pie == nil){
            pie = PNPieChart(frame: CGRect(x: 10, y: 10, width: pieChart.frame.height*0.9, height: pieChart.frame.height*0.9), items: items)!
        }
        pie.setItems(items)
        pie.descriptionTextColor = .white
        pie.descriptionTextFont = UIFont(name: "Avenir-Medium", size: 8.0)
        pie.stroke()
        pieChart.addSubview(pie)
        pie.delegate = self
        pie.legendStyle = .stacked
        if let legend = pie.getLegendWithMaxWidth(300) {
            legend.frame = CGRect(x: pie.frame.maxX + 5, y: pie.frame.maxY - legend.frame.height, width: legend.frame.width, height: legend.frame.height)
                pieChart.addSubview(legend)
        }
    }
    
    func drawLineChart() {
        lineChart.setXLabels(["Jan", "Feb", "Mar", "Apr", "May", "Jun"], withWidth: 50)
        lineChart.axisWidth = 2
        lineChart.isShowCoordinateAxis = true
        lineChart.showYGridLines = true
        lineChart.showLabel = true
        let data1 = [10, 20, 15, 26, 17, 28]
        let data2 = [17, 22, 30, 33, 26, 50]
        let lineData1 = PNLineChartData()
        let lineData2 = PNLineChartData()
        lineData1.dataTitle = "Made"
        lineData2.dataTitle = "Used"
        lineData1.color = .blue
        lineData2.color = .red
        lineData1.itemCount = UInt(lineChart.xLabels.count)
        lineData2.itemCount = UInt(lineChart.xLabels.count)
        lineData1.getData = { index in
            let yValue = CGFloat(data1[Int(index)])
            return PNLineChartDataItem(y: yValue)
        }
        lineData2.getData = { index in
            let yValue = CGFloat(data2[Int(index)])
            return PNLineChartDataItem(y: yValue)
        }
        lineData1.inflexionPointColor = .black
        lineData1.inflexionPointStyle = .circle
        lineData1.inflexionPointWidth = 2
        lineData2.inflexionPointColor = .black
        lineData2.inflexionPointStyle = .circle
        lineData2.inflexionPointWidth = 2
//        lineData1.showPointLabel = true`
        
        lineChart.chartData = [lineData1, lineData2]
        lineChart.stroke()
        lineChart.showSmoothLines = true
        
        lineChart.legendStyle = .serial
        if let legend = lineChart.getLegendWithMaxWidth(320) {
            legend.frame = CGRect(x: lineChart.frame.maxX - legend.frame.width, y: (lineChart.frame.maxY + lineChart.frame.height)/2 + 5, width: legend.frame.width, height: legend.frame.height)
            lineView.addSubview(legend)
        }
    }
}

extension HomeViewController: PNChartDelegate {    
    @objc func segmentedControlValueChanged(segment: UISegmentedControl) {
        if segment.selectedSegmentIndex == 0 {
            showMadeChart()
        } else if segment.selectedSegmentIndex == 1 {
            showUsedChart()
        } else if segment.selectedSegmentIndex == 2 {
            showStoredChart()
        }
    }
    func showUsedChart() {
        drawPieChart(items: usedItems)
    }
    func showMadeChart() {
        drawPieChart(items: madeItems)
    }
    func showStoredChart() {
        drawPieChart(items: storedItems)
    }
    
    func userClickedOnBar(at barIndex: Int) {
        print("click bar")
    }
    func userClicked(onPieIndexItem pieIndex: Int) {
        print("click pie")
        let item = pie.items[pieIndex] as! PNPieChartDataItem
        let vc = DetailViewController(nibName: "DetailViewController", bundle: nil)
        vc.modalPresentationStyle = .overFullScreen
        vc.itemTitle = item.textDescription
        self.present(vc, animated: true, completion: nil)
    }
    func userClicked(onLinePoint point: CGPoint, lineIndex: Int) {
        print("click line")
    }
    func userClicked(onLineKeyPoint point: CGPoint, lineIndex: Int, pointIndex: Int) {
        print("click line key point")
    }
    func didUnselectPieItem() {
        print("unselect pie")
    }
}
