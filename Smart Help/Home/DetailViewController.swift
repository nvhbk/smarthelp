//
//  DetailViewController.swift
//  Smart Help
//
//  Created by Hiên Hiên on 6/2/19.
//  Copyright © 2019 SmartHelp. All rights reserved.
//

import UIKit
import PNChart

class DetailViewController: UIViewController {

    @IBOutlet weak var titleItem: UILabel!
    @IBOutlet weak var lineChart: PNLineChart!
    @IBOutlet weak var nameChart: UILabel!
    @IBOutlet weak var lineView: UIView!
    @IBAction func okBtn(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    var itemTitle: String?
    override func viewDidLoad() {
        super.viewDidLoad()
//        drawLineChart()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        drawLineChart()
    }
    
    func drawLineChart() {
        titleItem.text = itemTitle
        lineChart.setXLabels(["Jan", "Feb", "Mar", "Apr", "May", "Jun"], withWidth: 50)
        lineChart.axisWidth = 2
        lineChart.isShowCoordinateAxis = true
        lineChart.showYGridLines = true
        lineChart.showLabel = true
        let data1 = [Int.random(in: 10...30), Int.random(in: 10...30), Int.random(in: 10...30), Int.random(in: 10...30), Int.random(in: 10...30), Int.random(in: 10...30)]
        let lineData1 = PNLineChartData()
        lineData1.dataTitle = "Used"
        lineData1.color = .orange
        lineData1.itemCount = UInt(lineChart.xLabels.count)
        lineData1.getData = { index in
            let yValue = CGFloat(data1[Int(index)])
            return PNLineChartDataItem(y: yValue)
        }
        lineData1.inflexionPointColor = .black
        lineData1.inflexionPointStyle = .circle
        lineData1.inflexionPointWidth = 2
        lineChart.chartData = [lineData1]
        lineChart.stroke()
        lineChart.showSmoothLines = true
        
        lineChart.legendStyle = .serial
        if let legend = lineChart.getLegendWithMaxWidth(320) {
            legend.frame = CGRect(x: lineChart.frame.maxX - legend.frame.width, y: (lineChart.frame.maxY + lineChart.frame.height)/2 + 5, width: legend.frame.width, height: legend.frame.height)
            lineView.addSubview(legend)
        }
    }
    
}
