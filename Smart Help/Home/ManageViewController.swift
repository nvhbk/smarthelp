//
//  ViewController.swift
//  Smart Help
//
//  Created by thanh tuan on 6/1/19.
//  Copyright © 2019 SmartHelp. All rights reserved.
//

import UIKit
import SwiftChart
import UICircularProgressRing
class ManageViewController: UIViewController {

    @IBOutlet weak var chart: Chart!
    @IBOutlet weak var percentView: UICircularProgressRing!
    @IBAction func handleTapPercentView(_ sender: UITapGestureRecognizer) {
        print("Hahahaha")
    }
    
    var upValue: CGFloat = 0.5
    var data: [Double] = [0]
    override func viewDidLoad() {
        super.viewDidLoad()
//        let series = ChartSeries([0])
//        chart.add(series)
        Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (timer) in
            self.percentView.value = self.percentView.value + self.upValue
            if self.percentView.value == 45 || self.percentView.value == 23 {
                self.upValue = -self.upValue
            }
            let random = Double.random(in: 0...360)
            let sintx = sin(random)
            self.data.append(sintx)
            if self.data.count < 100 {
                let series = ChartSeries(self.data)
                self.chart.add(series)
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        data.removeAll()
        chart.removeAllSeries()
    }

}

