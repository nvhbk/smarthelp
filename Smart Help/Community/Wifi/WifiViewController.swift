//
//  WifiViewController.swift
//  Smart Help
//
//  Created by thanh tuan on 6/2/19.
//  Copyright © 2019 SmartHelp. All rights reserved.
//

import UIKit
import  UICircularProgressRing

class WifiViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var percentView: UICircularProgressRing!
    @IBOutlet weak var messageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        animate()
    }
    
    func animate() {
        UIView.animate(withDuration: 0.5, delay: 0, options: [.autoreverse , .repeat], animations: {
            self.imageView.alpha = 0
        }, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.messageLabel.text = "CONNECTING"
        Timer.scheduledTimer(withTimeInterval: 5, repeats: false) { (timer) in
            self.setupAnimation()
        }
    }
    
    func setupAnimation() {
        self.messageLabel.text = "SET UP YOUR FEATURES"
        self.imageView.layer.removeAllAnimations()
        self.view.layoutIfNeeded()
        self.imageView.alpha = 1
        self.percentView.isHidden = false
        Timer.scheduledTimer(withTimeInterval: 0.05, repeats: true) { (timer) in
            self.percentView.value = self.percentView.value + 1
            if self.percentView.value == 100 {
                timer.invalidate()
                self.messageLabel.text = "SET UP COMPLETED"
            }
        }
    }
}
