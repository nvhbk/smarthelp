//
//  MapViewController.swift
//  Smart Help
//
//  Created by thanh tuan on 6/1/19.
//  Copyright © 2019 SmartHelp. All rights reserved.
//

import UIKit
import GoogleMaps
import YXWaveView
import Floaty

class MapViewController: UIViewController {
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var waveView: UIView!
    
    @IBOutlet weak var floatyButton: Floaty!
    var currentLocation: CLLocationCoordinate2D?
    private var isFetchedCurrentLocation: Bool = false
    var isGotoMyLocation = true
    private var polyline: GMSPolyline!
    private let locationManager: CLLocationManager = {
        $0.requestWhenInUseAuthorization()
        $0.desiredAccuracy = kCLLocationAccuracyBest
        $0.startUpdatingLocation()
        $0.startUpdatingHeading()
        return $0
    }(CLLocationManager())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocation()
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        mapView.delegate = self
        
        let someData = [(10.772042, 106.657537, 1232.0),
                        (10.771536, 106.659683, 999.0),
                        (10.770229, 106.657881, 1441.2),
                        (10.767067, 106.659232, 1111.7),
                        (10.772337, 106.655241, 1232.0)]
        
        
        loadMarker(data: someData)
        setupWave()
        setupFloatButton()
    }
    
    
    
    func setupFloatButton() {
        floatyButton.addItem("", icon: UIImage(named: "ic-graph")) { (item) in
            let popupStoryboard = UIStoryboard.init(name: "Popup", bundle: nil)
            let vc = popupStoryboard.instantiateViewController(withIdentifier: "GraphInforViewController")
            self.present(vc, animated: true, completion: nil)
            
        }
    }
    
    func setupWave() {
        let frame = CGRect(x: 0, y: 30, width: self.view.bounds.size.width, height: 30)
        let waterView = YXWaveView(frame: frame, color: UIColor.white)
        waterView.backgroundColor = UIColor(red: 248/255, green: 64/255, blue: 87/255, alpha: 1)
        waveView.addSubview(waterView)
        
        waterView.start()
    }
    
    func setupLocation(){
        locationManager.delegate = self
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                locationManager.requestWhenInUseAuthorization()
            case .restricted, .denied:
                break
            case .authorizedAlways, .authorizedWhenInUse:
                break
            }
        }
    }
    
    func presentSetting() {
        let alert = UIAlertController(title: "APP NEED PERMISSION", message: "NEED LOCATION PERMISSION", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OPEN SETTING", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL.init(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(URL.init(string: UIApplication.openSettingsURLString)!)
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    

    func loadMarker(data: [(Double, Double, Double)]) {
        mapView.clear()
        for object in data {
            let markerView = MarkerView(frame: CGRect(x: 0, y: 0, width: 180, height: 46))
            markerView.loadData(price: object.2)
            let position = CLLocationCoordinate2D(latitude: object.0, longitude: object.1)
            let marker = GMSMarker(position: position)
            marker.iconView = markerView
            marker.map = mapView
        }
        
        let homeData = [(10.767678, 106.655748),
                        (10.769217, 106.659074),
                        (10.774751, 106.657197),
                        (10.773349, 106.658838)]
        
        for object in homeData {
            let position = CLLocationCoordinate2D(latitude: object.0, longitude: object.1)
            let marker = GMSMarker(position: position)
            marker.icon = UIImage(named: "ic-home-green")
            marker.map = mapView
        }
        
        let position = CLLocationCoordinate2D(latitude: 10.771082, longitude: 106.658256)
        let marker = GMSMarker(position: position)
        marker.icon = UIImage(named: "ic-home-red")
        marker.map = mapView
    }
    
    func goToMyLocation() {
        if let currentLocation = self.currentLocation {
            mapView.animate(toLocation: currentLocation)
            mapView.animate(toZoom: 16)
        }
    }
    @IBAction func lightTap(_ sender: UIButton) {
        let someData = [(10.772042, 106.657537, 1232.0),
                        (10.771536, 106.659683, 999.0),
                        (10.770229, 106.657881, 1441.2),
                        (10.767067, 106.659232, 1111.7),
                        (10.772337, 106.655241, 1232.0)]
        
        
        loadMarker(data: someData)
        let popupStoryboard = UIStoryboard.init(name: "Popup", bundle: nil)
        let vc = popupStoryboard.instantiateViewController(withIdentifier: "FindViewController")
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func shareTap(_ sender: UIButton) {
        mapView.clear()
        
        let position = CLLocationCoordinate2D(latitude: 10.771082, longitude: 106.658256)
        let marker = GMSMarker(position: position)
        marker.icon = UIImage(named: "ic-home-green")
        marker.map = mapView
        
        let position1 = CLLocationCoordinate2D(latitude: 10.771868, longitude: 106.654590)
        let marker1 = GMSMarker(position: position1)
        marker1.icon = UIImage(named: "ic-home-red")
        marker1.map = mapView
        drawNavigationRouter(from: CLLocationCoordinate2D(latitude: 10.771082, longitude: 106.658256), to: CLLocationCoordinate2D(latitude: 10.771868, longitude: 106.654590))
        mapView.animate(to: GMSCameraPosition(latitude: 10.771082, longitude: 106.658256, zoom: 16))
        let popupStoryboard = UIStoryboard.init(name: "Popup", bundle: nil)
        let vc = popupStoryboard.instantiateViewController(withIdentifier: "SharePopupViewController")
        present(vc, animated: true, completion: nil)
    }
    
    func drawNavigationRouter(from: CLLocationCoordinate2D, to: CLLocationCoordinate2D){
        getPolylineRoute(from: from, to: to) {[weak self] (source, polyPath) in
            if let path = polyPath, let `self` = self {
                
                let bound = GMSCoordinateBounds(coordinate: from, coordinate: to)
                let camera = self.mapView.camera(for: bound, insets: UIEdgeInsets(top: 24, left: 24, bottom: 24, right: 24))
                self.mapView.animate(to: camera!)
                self.showPath(polyStr: path)
            }
        }
    }
    func getPolylineRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D, onComplete: @escaping (_ source: CLLocationCoordinate2D, _ polyString: String?) ->()){
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=false&mode=driving&key=AIzaSyBsqeDEYprebM-V0NtPNIgvv7Ym7Y8JQe8")!
        debugPrint("%@", url)
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            }else{
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                        
                        let routes = json["routes"] as? [Any]
                        let firstRoute = routes?[0] as? [String: Any]
                        let overview_polyline = firstRoute?["overview_polyline"] as? [String: Any]
                        let polyString = overview_polyline?["points"] as? String
                        DispatchQueue.main.async {
                            onComplete(source ,polyString)
                        }
                    }
                    
                }catch{
                    print("error in JSONSerialization")
                }
            }
        })
        
        task.resume()
    }
    func showPath(polyStr :String){
        let path = GMSPath(fromEncodedPath: polyStr)
        if (polyline == nil){
            polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 5.0
            polyline.strokeColor = UIColor(red: 248/255, green: 64/255, blue: 87/255, alpha: 1)
            polyline.map = self.mapView
        }else {
            polyline.path = path
        }
    }
    
    @IBAction func receiveTap(_ sender: UIButton) {
        let popupStoryboard = UIStoryboard.init(name: "Popup", bundle: nil)
        let vc = popupStoryboard.instantiateViewController(withIdentifier: "ReceiverPopupViewController")
        present(vc, animated: true, completion: nil)
    }
    
}
extension MapViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "PopupViewController") as? PopupViewController else { return false }
        present(vc, animated: true, completion: nil)
        return false
    }
}

extension MapViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        self.currentLocation = locValue
        if !isFetchedCurrentLocation {
            isFetchedCurrentLocation = true
            if isGotoMyLocation {
                goToMyLocation()
            }
            
            locationManager.stopUpdatingLocation()
        }
    }
}
