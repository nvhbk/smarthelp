//
//  ReceiverPopupViewController.swift
//  Smart Help
//
//  Created by thanh tuan on 6/2/19.
//  Copyright © 2019 SmartHelp. All rights reserved.
//

import UIKit
import YXWaveView

class ReceiverPopupViewController: UIViewController {
    @IBOutlet weak var waveView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupWave()
        // Do any additional setup after loading the view.
    }
    func setupWave() {
        let frame = CGRect(x: 0, y: 0, width: self.waveView.bounds.size.width, height: 30)
        let waterView = YXWaveView(frame: frame, color: UIColor(red: 43/255, green: 195/255, blue: 102/255, alpha: 1))
//        waterView.
        waterView.backgroundColor = .white
        waveView.addSubview(waterView)
        
        waterView.start()
    }

    @IBAction func okTapTap(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
