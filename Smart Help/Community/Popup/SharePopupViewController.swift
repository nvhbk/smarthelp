//
//  SharePopupViewController.swift
//  Smart Help
//
//  Created by thanh tuan on 6/2/19.
//  Copyright © 2019 SmartHelp. All rights reserved.
//

import UIKit

class SharePopupViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func shareTap(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
}
