//
//  GraphInforViewController.swift
//  Smart Help
//
//  Created by thanh tuan on 6/2/19.
//  Copyright © 2019 SmartHelp. All rights reserved.
//

import UIKit
import SwiftChart

class GraphInforViewController: UIViewController {
    @IBOutlet weak var chart: Chart!
    let dataIn: [Double] = [52, 106, 93, 99, 60, 50, 51, 87, 92, 98]
    let dataOut: [Double] = [10, 45, 25, 24, 28, 32, 14, 22, 20, 52]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let seriesIn = ChartSeries(dataIn)
        let seriesOut = ChartSeries(dataOut)
        seriesIn.color = .red
//        seriesIn.line = false
//        seriesOut.line = false
        chart.add([seriesIn, seriesOut])
    }
    
    @IBAction func taptap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}
