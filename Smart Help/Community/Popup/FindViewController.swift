//
//  FindViewController.swift
//  Smart Help
//
//  Created by thanh tuan on 6/2/19.
//  Copyright © 2019 SmartHelp. All rights reserved.
//

import UIKit
class FindViewController: UIViewController {
    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    var index = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let pulseEffect = LFTPulseAnimation(repeatCount: Float.infinity, radius:20, position:circleView.center)
        view.layer.insertSublayer(pulseEffect, below: circleView.layer)
        pulseEffect.backgroundColor = UIColor(red: 43/255, green: 195/255, blue: 02/255, alpha: 0.7).cgColor
        pulseEffect.radius = 250
        
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) { (timer) in
            switch self.index % 3 {
            case 0:
                self.messageLabel.text = "FINDING NEARBY ENERGY\n."
            case 1:
                self.messageLabel.text = "FINDING NEARBY ENERGY\n.."
            case 2:
                self.messageLabel.text = "FINDING NEARBY ENERGY\n..."
                
            default:
                break
            }
            
            self.index = self.index + 1
        }
    }
    

    @IBAction func taptap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
